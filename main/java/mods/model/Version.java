package mods.model;

import java.util.List;

import mods.application.version.VersionMap;
import mods.util.TimestampFormatter;

import org.q.toolkit.lang.Check;
import org.q.toolkit.lang.CollectionCheck;
import org.q.toolkit.lang.Str;
import org.q.toolkit.lang.collection.NewCollection;
import org.q.toolkit.lang.collection.NullPolicyType;

public class Version {

	private final String name;
	private final long created;

	private final List<RepositoryFile> files = NewCollection.list(NullPolicyType.REJECT);

	public Version(String name, long timestamp, List<RepositoryFile> files) {
		Check.notNull(name, timestamp);
		CollectionCheck.notEmpty(files);

		this.name = name;
		this.created = timestamp;
		this.files.addAll(files);
	}

	public String getName() {
		return name;
	}

	public long getCreated() {
		return created;
	}

	public List<RepositoryFile> getFiles() {
		return files;
	}

	public VersionMap getVersionMap() {
		return new VersionMap(files);
	}

	public String getStrippedName() {
		return name.replaceAll("[^a-zA-Z0-9_.-]", "");
	}

	@Override
	public String toString() {
		return Str.sub("$ || $", TimestampFormatter.format(created), name);
	}

}
