package mods.model;

import org.q.toolkit.lang.Str;

public class RepositoryFile {

	private String id;
	private String storageName;
	private String originalPath;
	private String md5;
	private long size;
	private long modified;

	public long getModified() {
		return modified;
	}

	public void setModified(long modified) {
		this.modified = modified;
	}

	public String getId() {
		return id;
	}

	public void setId(String repoId) {
		this.id = repoId;
	}

	public String getOriginalPath() {
		return originalPath;
	}

	public void setOriginalPath(String path) {
		this.originalPath = path;
	}

	public String getStorageName() {
		return storageName;
	}

	public void setStorageName(String name) {
		this.storageName = name;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return Str.sub("$[$]", id, originalPath);
	}
}