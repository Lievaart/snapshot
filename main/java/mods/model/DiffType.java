package mods.model;

public enum DiffType {

	MODIFIED, DELETED, ADDED;
}
