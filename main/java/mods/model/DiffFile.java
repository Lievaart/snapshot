package mods.model;

import org.q.toolkit.lang.Check;

public class DiffFile {

	private RepositoryFile original;
	private RepositoryFile updated;

	public DiffFile(RepositoryFile original, RepositoryFile updated) {
		Check.isTrue(original != null || updated != null, "original and updated file cannot both be <null>");

		this.original = original;
		this.updated = updated;
	}

	public RepositoryFile getOriginalFile() {
		return original;
	}

	public RepositoryFile getUpdatedFile() {
		return updated;
	}

	public DiffType getDiffType() {
		if (original == null) {
			return DiffType.ADDED;
		}
		if (updated == null) {
			return DiffType.DELETED;
		}
		return DiffType.MODIFIED;
	}

	@Override
	public String toString() {
		switch (getDiffType()) {
		case ADDED:
			return "ADD: " + updated.getOriginalPath();
		case MODIFIED:
			return "MOD: " + updated.getOriginalPath();
		case DELETED:
			return "DEL: " + original.getOriginalPath();
		}
		return super.toString();
	}

}
