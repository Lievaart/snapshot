package mods.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimestampFormatter {

	private static final String FORMAT = "dd-MMM | HH:mm";
	private static final String FORMAT_FILE = "yyyy-MM-dd--HH-mm-ss";

	public static String format(long timestamp) {
		return new SimpleDateFormat(FORMAT).format(new Date(timestamp));
	}

	public static String formatFileNameFriendly(long timestamp) {
		return new SimpleDateFormat(FORMAT_FILE).format(new Date(timestamp));
	}
}
