package mods.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.io.IOUtils;

public class MD5IO {

	public static String copy(File from, File to) throws IOException {
		Path path = Paths.get(from.getAbsolutePath());
		MessageDigest md = createMd5Digest();

		try (DigestInputStream dis = new DigestInputStream(Files.newInputStream(path), md)) {
			FileOutputStream os = new FileOutputStream(to);
			try {
				IOUtils.copy(dis, os);
			} finally {
				IOUtils.closeQuietly(os);
			}
		}
		return toMD5String(md.digest());

	}

	public static String read(File file) throws IOException {
		Path path = Paths.get(file.getAbsolutePath());
		MessageDigest md = createMd5Digest();

		try (DigestInputStream dis = new DigestInputStream(Files.newInputStream(path), md)) {
			int read = 0;
			byte[] buffer = new byte[1024 * 10];
			while (read != -1) {
				read = dis.read(buffer);
			}
		}
		return toMD5String(md.digest());
	}

	private static MessageDigest createMd5Digest() {
		try {
			return MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	private static String toMD5String(byte[] digest) {
		return new BigInteger(1, digest).toString(16);
	}
}
