package mods.util;

import org.q.toolkit.lang.Str;

public class OperationTimer {

	private long start = System.currentTimeMillis();

	public void printResult(String format) {
		long seconds = (System.currentTimeMillis() - start) / 1000;
		System.out.println(Str.sub(format, seconds));
	}
}
