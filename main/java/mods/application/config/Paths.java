package mods.application.config;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.q.toolkit.io.ResourceTool;
import org.q.toolkit.io.RuntimeIOException;
import org.q.toolkit.lang.collection.NewCollection;
import org.q.toolkit.lang.collection.NullPolicyType;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class Paths {

	private List<File> locations = NewCollection.list(NullPolicyType.REJECT);
	private File repo;

	@Inject
	public Paths(Application application) {
		repo = application.getRepoDir();

		for (String line : readSourcePaths()) {
			if (!StringUtils.isBlank(line)) {
				System.out.println("adding source dir: " + line.trim());
				File file = new File(line.trim());
				if (!file.isDirectory()) {
					throw new RuntimeIOException("% is not a directory", line);
				}
				locations.add(file);
			}
		}
	}

	private List<String> readSourcePaths() {
		try {
			return IOUtils.readLines(ResourceTool.getInputStream("/paths.txt"));
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}

	public List<File> getLocationDirs() {
		return Collections.unmodifiableList(locations);
	}

	public File getRepositoryDir() {
		return repo;
	}

	public File getRepositoryVersionsDir() {
		return new File(repo, "versions");
	}

	public File getRepositoryFilesDir() {
		return new File(repo, "files");
	}

	public File getRepositoryDatabaseFile() {
		return new File(getRepositoryDir(), "database.csv");
	}

	public File getRepositoryLockFile() {
		return new File(getRepositoryDir(), ".lock");
	}

	public File getRepositorySaveGameBackupDir() {
		return new File(getRepositoryDir(), "save-backup");
	}
}
