package mods.application.config;

import java.io.File;

import org.apache.commons.lang3.Validate;
import org.q.toolkit.io.ResourceTool;
import org.q.toolkit.lang.Str;
import org.q.toolkit.lang.collection.ValidatingMap;

public class Application {

	private File repoDir;
	private File saveGameDir;

	public Application() {
		ValidatingMap<String, String> props = ResourceTool.loadProperties("/application.properties");
		repoDir = new File(props.get("repo"));
		saveGameDir = new File(props.get("save"));

		if (repoDir.exists()) {
			Validate.isTrue(repoDir.isDirectory(), repoDir + " is not a directory!");
		} else {
			System.out.println(Str.sub("% does not exist, creating", repoDir));
			Validate.isTrue(repoDir.mkdirs(), repoDir + " could not be created!");
		}
	}

	public File getRepoDir() {
		return repoDir;
	}

	public File getSaveGameDir() {
		return saveGameDir;
	}

}
