package mods.application.save;

import java.io.File;
import java.io.IOException;

import mods.application.config.Application;
import mods.application.config.Paths;

import org.apache.commons.io.FileUtils;

import com.google.inject.Inject;

public class SaveGames {

	@Inject
	private Paths paths;
	@Inject
	private Application application;

	public void createBackup(String folder) throws IOException {
		File destinationDir = new File(paths.getRepositorySaveGameBackupDir(), folder);
		destinationDir.mkdirs();

		File saveGameDir = application.getSaveGameDir();
		if (saveGameDir != null && !saveGameDir.isDirectory()) {
			throw new IOException("Invalid save game dir: " + saveGameDir);
		}
		for (File file : saveGameDir.listFiles()) {
			File destination = new File(destinationDir, file.getName());
			System.out.println("BAK: " + file);
			if (file.isFile()) {
				FileUtils.copyFile(file, destination);
			} else {
				FileUtils.copyDirectory(file, destination);
			}
		}
	}
}
