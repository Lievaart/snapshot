package mods.application.version;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Observer;
import java.util.Set;

import mods.application.config.Paths;
import mods.application.file.Repository;
import mods.application.file.RepositoryLock;
import mods.model.DiffFile;
import mods.model.RepositoryFile;
import mods.model.Version;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.q.toolkit.io.RuntimeIOException;
import org.q.toolkit.lang.ObservableDelegate;
import org.q.toolkit.lang.Str;
import org.q.toolkit.lang.collection.NewCollection;
import org.q.toolkit.lang.collection.NullPolicyType;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class VersionManager {

	private static final boolean TEMP = true;
	private static final boolean PERSIST = false;

	@Inject
	private Paths paths;
	@Inject
	private Repository repo;

	private List<Version> versions = NewCollection.list(NullPolicyType.REJECT);
	private ObservableDelegate observable = new ObservableDelegate();
	private VersionIO io;

	@Inject
	public VersionManager(VersionIO io, RepositoryLock lock) {
		this.io = io;
		lock.checkHasLock();

		try {
			versions.addAll(io.loadVersions());
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}

	public void createAndStoreVersion(String name) throws IOException {
		System.out.println("Creating version: " + name);

		Version version = createVersion(name, PERSIST);
		versions.add(version);
		io.saveVersion(version);

		System.out.println(Str.sub("Version stored: $", name));
		observable.changeAndnotifyObservers();
	}

	private Version createVersion(String name, boolean temp) throws IOException {
		List<File> files = NewCollection.list(NullPolicyType.REJECT);

		for (File root : paths.getLocationDirs()) {
			files.addAll(FileUtils.listFiles(root, FileFilterUtils.trueFileFilter(), FileFilterUtils.trueFileFilter()));
		}

		List<RepositoryFile> repositoryFiles = NewCollection.list(NullPolicyType.REJECT);
		for (File file : files) {
			RepositoryFile stored = temp ? repo.createTempRepositoryFile(file) : repo.storeInRepository(file);
			repositoryFiles.add(stored);
		}
		return new Version(name, System.currentTimeMillis(), repositoryFiles);
	}

	public void addObserver(Observer o) {
		observable.addObserver(o);
		observable.changeAndnotifyObservers();
	}

	public List<Version> getVersions() {
		return Collections.unmodifiableList(versions);
	}

	/**
	 * @param indices
	 *            have to be ordered low to high
	 */
	public void deleteVersions(int[] indices) throws IOException {
		List<Version> remove = removeIndices(indices);

		for (Version version : remove) {
			io.delete(version);
		}
		repo.purge(getUnusedStorageLocations(remove));

		observable.changeAndnotifyObservers();
	}

	private Set<String> getUnusedStorageLocations(List<Version> remove) {
		Set<String> storageLocationsInUse = getStorageLocationsInUse();
		Set<String> unusedStorageLocations = new HashSet<String>();

		for (Version version : remove) {
			for (RepositoryFile file : version.getFiles()) {
				if (!storageLocationsInUse.contains(file.getStorageName())) {
					unusedStorageLocations.add(file.getStorageName());
				}
			}
		}
		return unusedStorageLocations;
	}

	private Set<String> getStorageLocationsInUse() {
		Set<String> usedStorageLocations = new HashSet<String>();
		for (Version version : versions) {
			for (RepositoryFile file : version.getFiles()) {
				usedStorageLocations.add(file.getStorageName());
			}
		}
		return usedStorageLocations;
	}

	private List<Version> removeIndices(int[] indices) {
		ArrayUtils.reverse(indices);
		List<Version> removed = NewCollection.list(NullPolicyType.REJECT);
		for (int index : indices) {
			removed.add(versions.remove(index));
		}
		return removed;
	}

	public Version createTempVersion() throws IOException {
		return createVersion("temp", TEMP);
	}

	public void load(Version load) throws IOException {
		VersionComparison compare = new VersionComparison(createTempVersion(), load);
		applyChangeSet(compare.getChangeList());
	}

	public void applyChangeSet(List<DiffFile> changeSet) throws IOException {
		// Original File => File currently in installation
		// Updated File => File in version we are loading (updating to)
		for (DiffFile change : changeSet) {
			switch (change.getDiffType()) {
			case DELETED:
				System.out.println("DEL: " + change.getOriginalFile().getOriginalPath());
				FileUtils.deleteQuietly(new File(change.getOriginalFile().getOriginalPath()));
				continue;

			case ADDED:
			case MODIFIED:
				try {
					System.out.println("RES: " + change.getUpdatedFile().getOriginalPath());
					File file = new File(change.getUpdatedFile().getOriginalPath());
					FileUtils.copyFile(repo.getStorageFile(change.getUpdatedFile()), file);
					file.setLastModified(change.getUpdatedFile().getModified());
					continue;
				} catch (Exception e) {
					System.err.println("restore failed:" + e.getMessage());
				}
			}
		}
	}

}
