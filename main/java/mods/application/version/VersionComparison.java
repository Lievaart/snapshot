package mods.application.version;

import java.util.Collections;
import java.util.List;

import mods.model.DiffFile;
import mods.model.RepositoryFile;
import mods.model.Version;

import org.q.toolkit.lang.Check;
import org.q.toolkit.lang.collection.NewCollection;

public class VersionComparison {

	private List<DiffFile> diff = NewCollection.list();
	private Version compareFrom;
	private Version compareTo;
	private VersionMap mapCompareFrom;
	private VersionMap mapCompareTo;

	public VersionComparison(Version compareFrom, Version compareTo) {
		Check.notNull(compareFrom, compareTo);
		this.compareFrom = compareFrom;
		this.compareTo = compareTo;

		mapCompareFrom = compareFrom.getVersionMap();
		mapCompareTo = compareTo.getVersionMap();

		findDeletedFiles();
		findModifiedFiles();
		findNewFiles();

	}

	private void findDeletedFiles() {
		for (RepositoryFile originalFile : compareFrom.getFiles()) {
			if (!mapCompareTo.containsPath(originalFile.getOriginalPath())) {
				diff.add(new DiffFile(originalFile, null));
			}
		}
	}

	private void findModifiedFiles() {
		for (RepositoryFile originalFile : compareFrom.getFiles()) {
			if (mapCompareTo.containsPath(originalFile.getOriginalPath()) && mapCompareTo.isModified(originalFile)) {
				RepositoryFile modifiedFile = mapCompareTo.getRepositoryFileForPath(originalFile.getOriginalPath());
				diff.add(new DiffFile(originalFile, modifiedFile));
			}
		}
	}

	private void findNewFiles() {
		for (RepositoryFile updatedFile : compareTo.getFiles()) {
			if (!mapCompareFrom.containsPath(updatedFile.getOriginalPath())) {
				diff.add(new DiffFile(null, updatedFile));
			}
		}
	}

	public List<DiffFile> getChangeList() {
		return Collections.unmodifiableList(diff);
	}

	public List<DiffFile> filterOnApplicableChanges(Version filter) {
		List<DiffFile> filtered = NewCollection.list();
		VersionMap map = filter.getVersionMap();

		for (DiffFile change : getChangeList()) {
			switch (change.getDiffType()) {

			case DELETED:
				if (map.containsPath(change.getOriginalFile().getOriginalPath())) {
					filtered.add(change);
				}
				continue;

			case ADDED:
			case MODIFIED:
				RepositoryFile onFS = map.getRepositoryFileForPath(change.getUpdatedFile().getOriginalPath());
				if (onFS == null || change.getUpdatedFile().getMd5() != onFS.getMd5()) {
					filtered.add(change);
				}
			}
		}
		return filtered;
	}

	public int count() {
		return diff.size();
	}
}
