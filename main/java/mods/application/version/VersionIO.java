package mods.application.version;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import mods.application.config.Paths;
import mods.application.file.Repository;
import mods.model.RepositoryFile;
import mods.model.Version;

import org.apache.commons.io.FileUtils;
import org.q.toolkit.io.PropertiesIO;
import org.q.toolkit.lang.Check;
import org.q.toolkit.lang.collection.NewCollection;
import org.q.toolkit.lang.collection.NullPolicyType;

import com.google.inject.Inject;

public class VersionIO {

	private static final String EXT = "ini";

	@Inject
	private Repository repository;

	private final Paths paths;

	@Inject
	public VersionIO(Paths paths) {
		this.paths = paths;

		File storage = paths.getRepositoryVersionsDir();
		storage.mkdirs();
		Check.isTrue(storage.isDirectory(), "% is not a directory", storage);
	}

	public List<Version> loadVersions() throws IOException {
		List<Version> versions = NewCollection.list(NullPolicyType.REJECT);

		for (File file : FileUtils.listFiles(paths.getRepositoryVersionsDir(), new String[] { EXT }, true)) {
			versions.add(versionFromProperties(PropertiesIO.load(file)));
		}
		return versions;
	}

	private Version versionFromProperties(Properties props) {
		String name = props.remove("name").toString();
		String timestamp = props.remove("created").toString();
		Check.matches(timestamp, "\\d++");

		List<RepositoryFile> files = NewCollection.list(NullPolicyType.REJECT);
		for (String key : props.stringPropertyNames()) {
			files.add(repository.getRepositoryFileForId(key));
		}
		return new Version(name, Long.valueOf(timestamp), files);
	}

	public void saveVersion(Version version) throws IOException {
		PropertiesIO.store(convertToProperties(version), getFile(version));
	}

	private File getFile(Version version) {
		String name = version.getCreated() + "-" + version.getStrippedName();
		return new File(paths.getRepositoryVersionsDir(), name + "." + EXT);
	}

	private Properties convertToProperties(Version version) {
		Properties props = new Properties();
		props.setProperty("name", version.getName());
		props.setProperty("created", version.getCreated() + "");

		List<RepositoryFile> files = version.getFiles();
		for (int i = 0; i < files.size(); i++) {
			props.setProperty(files.get(i).getId(), "");
		}
		return props;
	}

	public void delete(Version version) {
		FileUtils.deleteQuietly(getFile(version));
	}
}
