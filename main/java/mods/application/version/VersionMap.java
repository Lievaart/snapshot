package mods.application.version;

import java.util.Hashtable;
import java.util.List;

import mods.model.RepositoryFile;

public class VersionMap {

	private Hashtable<String, RepositoryFile> pathToRepositoryFile = new Hashtable<String, RepositoryFile>();

	public VersionMap(List<RepositoryFile> files) {
		for (RepositoryFile file : files) {
			pathToRepositoryFile.put(file.getOriginalPath(), file);
		}
	}

	public boolean isModified(RepositoryFile vf) {
		String path = vf.getOriginalPath();
		if (!pathToRepositoryFile.containsKey(path)) {
			return true;
		}
		RepositoryFile other = pathToRepositoryFile.get(path);
		if (vf.getSize() != other.getSize()) {
			return true;
		}
		if (vf.getModified() == other.getModified()) {
			return false;
		}
		return !vf.getMd5().equals(other.getMd5());
	}

	public boolean containsPath(String path) {
		return pathToRepositoryFile.containsKey(path);
	}

	public RepositoryFile getRepositoryFileForPath(String path) {
		return pathToRepositoryFile.get(path);
	}

}
