package mods.application.file;

import java.io.File;
import java.io.IOException;

import mods.application.config.Paths;

import org.q.toolkit.lang.Check;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class RepositoryLock {

	private final boolean hasLock;

	@Inject
	public RepositoryLock(Paths paths) {
		hasLock = acquireLockFile(paths.getRepositoryLockFile());
	}

	private boolean acquireLockFile(File lockFile) {
		if (lockFile.exists()) {
			return false;
		}

		try {
			lockFile.getParentFile().mkdirs();
			lockFile.createNewFile();
			lockFile.deleteOnExit();
			return true;

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean hasLock() {
		return hasLock;
	}

	public void checkHasLock() {
		Check.isTrue(hasLock, "Lock file for repository not acquired yet!");
	}
}
