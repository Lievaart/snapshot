package mods.application.file;

import java.util.List;

import mods.model.RepositoryFile;

import org.apache.commons.lang3.StringUtils;
import org.q.toolkit.lang.StringEscape;
import org.q.toolkit.lang.collection.MapTool;
import org.q.toolkit.lang.collection.NewCollection;
import org.q.toolkit.lang.collection.NullPolicyType;
import org.q.toolkit.lang.pattern.PatternTool;

public class RepositoryFileConverter {

	private static StringEscape csv = new StringEscape(MapTool.of('c', ',', 'b', '\\'));

	public static String toCsv(RepositoryFile file) {
		List<String> cells = NewCollection.list(NullPolicyType.REJECT);

		cells.add(file.getId());
		cells.add(csv.escape(file.getOriginalPath()));
		cells.add(file.getSize() + "");
		cells.add(file.getMd5());
		cells.add(file.getModified() + "");
		cells.add(file.getStorageName());

		return StringUtils.join(cells, ',');
	}

	public static RepositoryFile toRepositoryFile(String raw) {
		String[] split = PatternTool.split(",", raw);

		RepositoryFile file = new RepositoryFile();

		file.setId(split[0]);
		file.setOriginalPath(csv.unescape(split[1]));
		file.setSize(Long.valueOf(split[2]));
		file.setMd5(split[3]);
		file.setModified(Long.valueOf(split[4]));
		file.setStorageName(split[5]);

		return file;
	}
}
