package mods.application.file;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mods.application.config.Paths;
import mods.model.RepositoryFile;
import mods.util.MD5IO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import org.q.toolkit.lang.Check;
import org.q.toolkit.lang.IdGenerator;
import org.q.toolkit.lang.Str;
import org.q.toolkit.lang.collection.MultiMap;
import org.q.toolkit.lang.collection.NewCollection;
import org.q.toolkit.lang.collection.NullPolicyType;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class Repository {

	private Paths paths;

	private IdGenerator ids = new IdGenerator();
	private Map<String, RepositoryFile> idToFile = new Hashtable<>();
	private MultiMap<String, RepositoryFile> pathToFiles = new MultiMap<>();

	@Inject
	public Repository(RepositoryLock lock, Paths paths) throws IOException {
		this.paths = paths;

		lock.checkHasLock();
		load();
		paths.getRepositoryFilesDir().mkdirs();
	}

	public RepositoryFile createTempRepositoryFile(File file) throws IOException {
		Validate.isTrue(file.isFile(), "not a file: " + file);

		RepositoryFile existing = getExistingFile(file);
		if (existing != null) {
			return existing;
		}

		RepositoryFile store = new RepositoryFile();
		store.setId(generateId());
		store.setModified(file.lastModified());
		store.setOriginalPath(file.getAbsolutePath());
		store.setSize(file.length());
		store.setMd5(MD5IO.read(file));

		return store;
	}

	public RepositoryFile storeInRepository(File file) throws IOException {
		Validate.isTrue(file.isFile(), "not a file: " + file);

		RepositoryFile existing = getExistingFile(file);
		if (existing != null) {
			return existing;
		}

		String id = generateId();

		RepositoryFile store = new RepositoryFile();
		store.setId(id);
		store.setModified(file.lastModified());
		store.setOriginalPath(file.getAbsolutePath());
		store.setSize(file.length());

		String md5 = MD5IO.read(file);
		store.setMd5(md5);
		RepositoryFile existingMd5 = getExistingFile(file, md5);

		if (existingMd5 == null) {
			System.out.println(Str.sub("new repo file: % < %", md5, store.getOriginalPath()));
			MD5IO.copy(file, getStorageFile(id));
			store.setStorageName(id);

		} else {
			System.out.println(Str.sub("duplicate md5: %", store.getOriginalPath()));
			store.setStorageName(existingMd5.getStorageName());
		}
		idToFile.put(id, store);
		pathToFiles.add(store.getOriginalPath(), store);
		save();
		return store;
	}

	private String generateId() {
		return System.currentTimeMillis() + "-" + ids.next();
	}

	private File getStorageFile(String id) {
		return new File(paths.getRepositoryFilesDir(), id);
	}

	private RepositoryFile getExistingFile(File query) {
		for (RepositoryFile file : pathToFiles.get(query.getAbsolutePath())) {
			if (file.getSize() != query.length()) {
				continue;
			}
			if (file.getModified() == query.lastModified()) {
				return file;
			}
		}
		return null;
	}

	private RepositoryFile getExistingFile(File query, String md5) {
		for (RepositoryFile file : pathToFiles.get(query.getAbsolutePath())) {
			if (file.getSize() != query.length()) {
				continue;
			}
			if (file.getMd5().equals(md5)) {
				return file;
			}
		}
		return null;
	}

	public RepositoryFile getRepositoryFileForId(String id) {
		return idToFile.get(id);
	}

	private File getFileForId(String id) {
		return new File(paths.getRepositoryFilesDir(), id);
	}

	public File getStorageFile(RepositoryFile restore) {
		File file = getFileForId(restore.getStorageName());
		Check.isTrue(file.isFile(), "% is not a file ($)", file, restore.getOriginalPath());
		return file;
	}

	private void load() throws IOException {
		File dbFile = paths.getRepositoryDatabaseFile();
		if (!dbFile.isFile()) {
			System.err.println("Warning: repository does not exist, creating empty repository");
			return;
		}
		for (String line : FileUtils.readLines(dbFile)) {
			RepositoryFile file = RepositoryFileConverter.toRepositoryFile(line);
			idToFile.put(file.getId(), file);
		}
		pathToFiles = indexPathToFiles();
	}

	private MultiMap<String, RepositoryFile> indexPathToFiles() {
		MultiMap<String, RepositoryFile> index = new MultiMap<>();
		for (RepositoryFile file : idToFile.values()) {
			index.add(file.getOriginalPath(), file);
		}
		return index;
	}

	public void save() throws IOException {
		List<String> output = NewCollection.list(NullPolicyType.REJECT);
		for (RepositoryFile file : idToFile.values()) {
			output.add(RepositoryFileConverter.toCsv(file));
		}
		FileUtils.writeLines(paths.getRepositoryDatabaseFile(), output);
	}

	public RepositoryFile getVersionedFile(String id) {
		return idToFile.get(id);
	}

	public Set<String> getFilesInStorageIndex() {
		Set<String> ids = new HashSet<String>();
		for (RepositoryFile file : idToFile.values()) {
			ids.add(file.getStorageName());
		}
		return ids;
	}

	public void dumpDuplicateMd5() {
		for (String path : pathToFiles.keySet()) {
			MultiMap<String, String> md5ToStorage = new MultiMap<String, String>();

			for (RepositoryFile fileInPath : pathToFiles.get(path)) {
				md5ToStorage.add(fileInPath.getMd5(), fileInPath.getStorageName());
			}
			Set<String> md5s = md5ToStorage.keySet();
			for (String md5 : md5s) {
				Set<String> storedAs = new HashSet<>(md5ToStorage.get(md5));
				if (storedAs.size() > 1) {
					System.out.println(storedAs);
				}
			}
		}
	}

	public void purge(Set<String> unusedStorageLocations) throws IOException {
		for (String id : unusedStorageLocations) {
			System.out.println("purge: " + id);
			getFileForId(id).delete();
			idToFile.remove(id);
		}
		pathToFiles = indexPathToFiles();
		save();
	}

	public void deletemeRemoveFromIndex(String name) throws IOException {
		idToFile.remove(name);
		pathToFiles = indexPathToFiles();
		save();
	}

}
