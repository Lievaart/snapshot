package mods.gui;

import java.util.Observable;
import java.util.Observer;

import mods.application.version.VersionManager;
import mods.model.Version;

import com.google.inject.Inject;

public class VersionManagerObserver implements Observer {

	@Inject
	private VersionManager manager;

	@Inject
	private VersionComponents components;

	@Override
	public void update(Observable obs, Object obj) {
		components.getVersionList().setListData(manager.getVersions().toArray(new Version[] {}));
	}

}
