package mods.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import mods.application.version.VersionManager;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class VersionController {

	private VersionComponents components;

	@Inject
	public VersionController(VersionComponents components, VersionListeners listeners) {
		this.components = components;

		JFrame frame = components.getFrame();
		JPanel buttonPanel = new JPanel(new GridLayout(1, 0));

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("save and restore versions");
		frame.setSize(800, 600);

		frame.getContentPane().add(new JScrollPane(components.getVersionList()), BorderLayout.CENTER);
		frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);

		buttonPanel.add(components.getCreateButton());
		buttonPanel.add(components.getDiffButton());
		buttonPanel.add(components.getLoadButton());
		buttonPanel.add(components.getChangesButton());
		buttonPanel.add(components.getDeleteButton());

		components.getCreateButton().setMnemonic('C');
		components.getLoadButton().setMnemonic('L');
		components.getDiffButton().setMnemonic('D');

		components.getCreateButton().addActionListener(listeners.getAddVersionListener());
		components.getLoadButton().addActionListener(listeners.getLoadListener());
		components.getDeleteButton().addActionListener(listeners.getDeleteListener());
		components.getDiffButton().addActionListener(listeners.getDiffListener());
		components.getChangesButton().addActionListener(listeners.getChangesListener());
	}

	@Inject
	public void observeVersionManager(VersionManager manager, VersionManagerObserver observer) {
		manager.addObserver(observer);
	}

	public void showVersionFrame() {
		components.getFrame().setVisible(true);
	}
}
