package mods.gui;

import java.awt.event.ActionListener;

import mods.gui.event.AddVersionListener;
import mods.gui.event.ChangeSetListener;
import mods.gui.event.DeleteListener;
import mods.gui.event.DiffListener;
import mods.gui.event.LoadListener;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class VersionListeners {

	@Inject
	private AddVersionListener addVersionListener;
	@Inject
	private LoadListener loadListener;
	@Inject
	private DeleteListener deleteListener;
	@Inject
	private DiffListener diffListener;
	@Inject
	private ChangeSetListener changeSetListener;

	public AddVersionListener getAddVersionListener() {
		return addVersionListener;
	}

	public LoadListener getLoadListener() {
		return loadListener;
	}

	public DeleteListener getDeleteListener() {
		return deleteListener;
	}

	public ActionListener getDiffListener() {
		return diffListener;
	}

	public ActionListener getChangesListener() {
		return changeSetListener;
	}
}
