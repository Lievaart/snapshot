package mods.gui;

import javax.swing.JOptionPane;

import mods.application.config.Paths;
import mods.application.file.RepositoryLock;

import com.google.inject.Inject;

public class LockController {

	@Inject
	private Paths paths;
	@Inject
	private RepositoryLock lock;

	public void init() {
		if (!lock.hasLock()) {
			StringBuilder builder = new StringBuilder();
			builder.append("<html>ERROR:");
			builder.append("The application is already running!<BR/>");
			builder.append("Unable to acquire lock file:<BR/>");
			builder.append(paths.getRepositoryLockFile().toString() + "<BR/>");
			JOptionPane.showMessageDialog(null, builder.toString());
		}
		lock.checkHasLock();

	}
}
