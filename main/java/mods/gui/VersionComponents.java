package mods.gui;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;

import mods.model.Version;

import com.google.inject.Singleton;

@Singleton
public class VersionComponents {

	private final JFrame frame = new JFrame();
	private final JList<Version> versionList = new JList<>();;
	private final JButton createButton = new JButton("Create Version");
	private final JButton loadButton = new JButton("Load");
	private final JButton deleteButton = new JButton("Delete");
	private final JButton diffButton = new JButton("Diff");
	private final JButton changesButton = new JButton("Apply Changeset");

	public JList<Version> getVersionList() {
		return versionList;
	}

	public JButton getLoadButton() {
		return loadButton;
	}

	public JFrame getFrame() {
		return frame;
	}

	public JButton getDiffButton() {
		return diffButton;
	}

	public JButton getDeleteButton() {
		return deleteButton;
	}

	public JButton getCreateButton() {
		return createButton;
	}

	public JButton getChangesButton() {
		return changesButton;
	}

}
