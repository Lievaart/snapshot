package mods.gui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JOptionPane;

import mods.application.version.VersionManager;

import org.apache.commons.lang3.StringUtils;

import com.google.inject.Inject;

public class DeleteListener implements ActionListener {

	@Inject
	private VersionManager versions;
	@Inject
	private ListSelection selection;

	@Override
	public void actionPerformed(ActionEvent ae) {
		if (!selection.selectedAtLeast(1)) {
			JOptionPane.showMessageDialog(null, "Please select at least one version.");
			return;
		}

		String message = JOptionPane.showInputDialog("Type 'delete' to confirm (without quotes)");
		if (!StringUtils.equalsIgnoreCase(message, "delete")) {
			return;
		}
		try {
			versions.deleteVersions(selection.getSelectedIndices());
			System.out.println("Delete completed");
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Something went wrong while deleting version.");
		}
	}
}
