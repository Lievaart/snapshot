package mods.gui.event;

import java.io.IOException;

import javax.swing.JList;
import javax.swing.ListModel;

import mods.application.version.VersionManager;
import mods.gui.VersionComponents;
import mods.model.Version;

import org.q.toolkit.lang.Check;

import com.google.inject.Inject;

public class ListSelection {

	@Inject
	private VersionComponents components;
	@Inject
	private VersionManager manager;

	public boolean isVersionListEmpty() {
		return components.getVersionList().getModel().getSize() < 1;
	}

	public Version getFirstSelectedOrLastInList() {
		Check.isFalse(isVersionListEmpty());
		return getSelectionCount() > 0 ? getFirstSelected() : getLastInList();
	}

	private Version getLastInList() {
		Check.isFalse(isVersionListEmpty());
		ListModel<Version> model = components.getVersionList().getModel();
		return model.getElementAt(model.getSize() - 1);
	}

	public Version getLastSelectedOrCreateTempVersion() throws IOException {
		Check.isFalse(isVersionListEmpty());
		return getSelectionCount() >= 2 ? getLastSelected() : manager.createTempVersion();
	}

	public Version getFirstSelected() {
		Check.isFalse(isVersionListEmpty());
		Check.isTrue(selectedAtLeast(1));

		return components.getVersionList().getSelectedValue();
	}

	public Version getLastSelected() {
		Check.isFalse(isVersionListEmpty());
		Check.isTrue(selectedAtLeast(1));

		JList<Version> list = components.getVersionList();
		int[] indices = list.getSelectedIndices();
		int lastIndex = indices[indices.length - 1];
		return list.getModel().getElementAt(lastIndex);
	}

	public int[] getSelectedIndices() {
		return components.getVersionList().getSelectedIndices();
	}

	public int getSelectionCount() {
		return getSelectedIndices().length;
	}

	public boolean selectedAtLeast(int count) {
		return getSelectionCount() >= count;
	}

	public boolean selectedAtMost(int count) {
		return getSelectionCount() <= count;
	}

}
