package mods.gui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;

import mods.application.version.VersionComparison;
import mods.model.DiffFile;
import mods.model.Version;

import org.q.toolkit.lang.Str;

import com.google.inject.Inject;

public class DiffListener implements ActionListener {

	@Inject
	private ListSelection selection;

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (selection.isVersionListEmpty()) {
			JOptionPane.showMessageDialog(null, "No versions available");
			return;
		}

		try {
			Version compareFrom = selection.getFirstSelectedOrLastInList();
			Version compareTo = selection.getLastSelectedOrCreateTempVersion();

			System.out.println(Str.sub("Comparing % to %", compareFrom.getName(), compareTo.getName()));
			VersionComparison diff = new VersionComparison(compareFrom, compareTo);
			showDiff(diff.getChangeList());
			System.out.println("Compare complete");

		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Compare Failed!");
			e.printStackTrace();
		}
	}

	private void showDiff(List<DiffFile> changes) {
		for (DiffFile change : changes) {
			System.out.println(change);
		}
	}

}
