package mods.gui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JOptionPane;

import mods.application.version.VersionManager;
import mods.util.OperationTimer;

import org.apache.commons.lang3.StringUtils;

import com.google.inject.Inject;

public class AddVersionListener implements ActionListener {

	@Inject
	private VersionManager versions;

	@Override
	public void actionPerformed(ActionEvent event) {
		OperationTimer timer = new OperationTimer();

		String version = JOptionPane.showInputDialog("Please enter version name");
		if (version == null) {
			return;
		}
		if (StringUtils.isBlank(version)) {
			JOptionPane.showMessageDialog(null, "version name cannot be blank!");
			return;
		}
		try {
			versions.createAndStoreVersion(version);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Unable to create version " + version);
			e.printStackTrace();
		}
		timer.printResult("Creating version took $ second(s)");
	}

}
