package mods.gui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JOptionPane;

import mods.application.save.SaveGames;
import mods.application.version.VersionManager;
import mods.model.Version;
import mods.util.OperationTimer;
import mods.util.TimestampFormatter;

import com.google.inject.Inject;

public class LoadListener implements ActionListener {

	@Inject
	private VersionManager manager;
	@Inject
	private SaveGames saveGames;
	@Inject
	private ListSelection selection;

	@Override
	public void actionPerformed(ActionEvent ae) {
		if (selection.isVersionListEmpty()) {
			JOptionPane.showMessageDialog(null, "No versions available");
			return;
		}
		if (!selection.selectedAtMost(1)) {
			JOptionPane.showMessageDialog(null, "Cannot load multiple versions at once");
			return;
		}
		Version load = selection.getFirstSelectedOrLastInList();

		try {
			String dateTime = TimestampFormatter.formatFileNameFriendly(System.currentTimeMillis());
			saveGames.createBackup(dateTime + "-" + load.getStrippedName());
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Loading version aborted; unable to backup save games.");
			e.printStackTrace();
			return;
		}
		loadVersion(load);
	}

	private void loadVersion(Version version) {
		OperationTimer timer = new OperationTimer();
		try {
			manager.load(version);
			System.out.println(version.getName() + " loaded");

		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Something went wrong while restoring version!");
			e.printStackTrace();
		}
		timer.printResult("Loading version took $ second(s)");
	}

}
