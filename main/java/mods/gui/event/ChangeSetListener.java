package mods.gui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;

import mods.application.version.VersionComparison;
import mods.application.version.VersionManager;
import mods.model.DiffFile;
import mods.model.Version;

import org.q.toolkit.lang.Str;

import com.google.inject.Inject;

public class ChangeSetListener implements ActionListener {

	@Inject
	private ListSelection selection;
	@Inject
	private VersionManager manager;

	@Override
	public void actionPerformed(ActionEvent ae) {
		if (!selection.selectedAtLeast(2)) {
			JOptionPane.showMessageDialog(null, "No change set selected! Select at least two versions.");
			return;
		}
		Version compareFrom = selection.getFirstSelected();
		Version compareTo = selection.getLastSelected();

		System.out.println(Str.sub("Creating change set % to %", compareFrom.getName(), compareTo.getName()));
		VersionComparison diff = new VersionComparison(compareFrom, compareTo);
		System.out.println(diff.count() + " changes found, filtering on current installation");

		try {
			Version temp = manager.createTempVersion();
			List<DiffFile> changes = diff.filterOnApplicableChanges(temp);
			for (DiffFile change : changes) {
				System.out.println(change);
			}
			System.out.println(changes.size() + " changes in total");
			applyChanges(diff, changes);

		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Unable to create changeset");
		}
	}

	private void applyChanges(VersionComparison diff, List<DiffFile> changes) {
		StringBuilder builder = new StringBuilder("<html>");

		builder.append(diff.count() + " changes in change set <br/>");
		builder.append(changes.size() + " applicable to current install <br/>");
		builder.append("<br/>");
		builder.append("Apply change set?");

		int result = JOptionPane.showConfirmDialog(null, builder.toString());
		if (result != JOptionPane.OK_OPTION) {
			return;
		}
		try {
			manager.applyChangeSet(changes);
			System.out.println("Apply change set complete");

		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Unable to apply change set!");
			e.printStackTrace();
			return;
		}
	}

}
