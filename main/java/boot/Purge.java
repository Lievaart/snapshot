package boot;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mods.application.config.Paths;
import mods.application.file.Repository;
import mods.application.version.VersionManager;
import mods.model.RepositoryFile;
import mods.model.Version;

import org.q.toolkit.lang.collection.NewCollection;
import org.q.toolkit.lang.collection.NullPolicyType;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Purge {

	public static void main(String[] args) throws IOException {
		Injector injector = Guice.createInjector();
		Repository repository = injector.getInstance(Repository.class);
		Paths paths = injector.getInstance(Paths.class);
		VersionManager manager = injector.getInstance(VersionManager.class);

		purgeIndexedFilesNotOnFilesystem(repository, paths);
		purgeIndexedFilesNotInVersion(repository, manager);
		purgeFilesNotInRepository(repository, paths);
	}

	private static void purgeIndexedFilesNotOnFilesystem(Repository repository, Paths paths) throws IOException {
		Map<String, File> onFileSystem = new Hashtable<>();
		for (File file : paths.getRepositoryFilesDir().listFiles()) {
			onFileSystem.put(file.getName(), file);
		}
		Set<String> indexed = repository.getFilesInStorageIndex();
		List<String> remove = NewCollection.list(NullPolicyType.REJECT);
		for (String name : indexed) {
			if (!onFileSystem.containsKey(name)) {
				remove.add(name);
				System.out.println("need to remove from index: " + name);
				repository.deletemeRemoveFromIndex(name);
			}
		}
		System.out.println(remove.size() + " to remove from index");
	}

	private static void purgeIndexedFilesNotInVersion(Repository repository, VersionManager manager) throws IOException {
		Set<String> indexed = repository.getFilesInStorageIndex();
		System.out.println(indexed.size() + " files in storage index");

		for (Version version : manager.getVersions()) {
			for (RepositoryFile repositoryFile : version.getFiles()) {
				if (repositoryFile == null) {
					Thread.yield();
				}
				String name = repositoryFile.getStorageName();
				indexed.remove(name);
			}
		}
		System.out.println(indexed.size() + " unused files in index, but not in version");
		repository.purge(indexed);
	}

	private static void purgeFilesNotInRepository(Repository repository, Paths paths) {
		Set<String> usedIds = repository.getFilesInStorageIndex();
		System.out.println(usedIds.size() + " files in storage index");

		List<File> deleteme = NewCollection.list(NullPolicyType.REJECT);
		for (File repoFile : paths.getRepositoryFilesDir().listFiles()) {
			if (!usedIds.contains(repoFile.getName())) {
				deleteme.add(repoFile);
			}
		}
		System.out.println(deleteme.size() + " files on filesystem, but not in index");
		for (File file : deleteme) {
			file.delete();
		}
	}
}
