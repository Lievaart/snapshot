package boot;

import mods.gui.LockController;
import mods.gui.VersionController;

import org.q.toolkit.swing.SwingThread;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Main {

	// TODO
	// UI revamp
	// + show status in JList
	// + show changes in JList
	// + move System.out to JList
	// + review package structure
	// Code review
	// Unit tests
	// mercurial

	public static void main(String[] args) {
		SwingThread.invokeLater(new Runnable() {
			@Override
			public void run() {
				Injector injector = Guice.createInjector();
				injector.getInstance(LockController.class).init();
				VersionController versions = injector.getInstance(VersionController.class);
				versions.showVersionFrame();
			}
		});
	}
}
