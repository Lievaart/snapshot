package boot;

import java.io.IOException;

import mods.application.file.Repository;

import com.google.inject.Guice;

public class FindDuplicateMd5 {

	public static void main(String[] args) throws IOException {
		Repository repository = Guice.createInjector().getInstance(Repository.class);
		repository.dumpDuplicateMd5();
	}
}
