package mods.util;

import org.joda.time.DateTime;
import org.junit.Test;
import org.q.toolkit.lang.Check;

public class TimestampFormatterU {

	@Test
	public void format() {
		int year = 1982;
		int month = 3;
		int day = 21;
		int hour = 12;
		int minutes = 35;

		DateTime date = new DateTime(year, month, day, hour, minutes);
		String formatted = TimestampFormatter.format(date.getMillis());
		Check.isEqual(formatted, "21-Mar | 12:35");
	}
}
